//definicje metod klas
//AUTOR: Mikolaj Zurowski
//NR INDEKSU: 226236

#include <iostream>
#include "klasy.hh"
using namespace std;

void Lista::Dodaj(int y) //dodawanie elementu do listy
{
  int x;
  for(int i=0;i<y;i++)
    {
      cout <<"Podaj liczbe do dodania: "<<endl;
      cin >>x;
      Dane *nowa= new Dane;
      nowa->wart=x;
      if(pierwszy==0)  //sprawdzamy czy dodajemy na poczatek
	{
	  pierwszy=nowa; //jesli tak->nowy element jest poczatkiem listy
	}
      else
	{
	  Dane *tmp=pierwszy; 
	  while(tmp->nast) //dopoki wskaznik na poczatek listy nie wskazuje na 
	    {              //NULL
	      tmp=tmp->nast;
	    }
	  tmp->nast=nowa;
	  nowa->nast=0;
	}
    }
}

void Lista::Pokaz()  //funkcja pokazywania zawartosci listy
{
  Dane *tmp=pierwszy;
  while(tmp)
    {
      cout << tmp->wart<<endl; //wyswietlamy wartosc danego elementu
      tmp=tmp->nast;           //przechodzimy na nastepny element
    }
}

void Lista::Usun(int nr) //usuwanie elementu o danym numerze z listy
{
  {
    // jezeli to pierwszy element listy
    if (nr==1)
    {
        Dane *temp = pierwszy;
        pierwszy = temp->nast; //poczatek listy, 1 el. wskazuje na 2 el. itd..
    }
 
    // jeżeli nie jest to pierwszy element
    if (nr>=2)
    {
        int j = 1;
 
        // do usuniecia srodkowego elemetnu potrzebujemy wskaznika na el n-1
        // wskaznik *temp bedzie wskaznikiem na el poprzedzajacy el usuwana
        Dane *temp = pierwszy;
 
        while (temp)
        {
            // sprawdzamy czy wskaznik jest na elemencie n-1 niz usuwana
            if ((j+1)==nr)
	      {
		break;
	      }
	    else
	      {
		// jezeli nie to przewijamy petle do przodu
		temp = temp->nast;
		j++;
	      }
        }
 
        // wskaznik *temp wskazuje teraz na element n-1
        // nadpisujemy wkaznik elementu n na element n+1
        // bezpowrotnie tracimy element n-ty
 
        // dodatkowo sprawdzamy czy aby nie jest to ostatni element
        // wtedy nalezy wyzerowac ostatni wskaznik
        if (temp->nast->nast==0)
	  {
            temp->nast = 0;
	  }
        // jezeli nie byl to ostatni element
        else
	  {
            temp->nast = temp->nast->nast;
	  }
    }
  }
}

void Lista::Czysc() //funkcja czyszczenia listy korzystajaca z funkcji usuwania
{
  while(pierwszy!=0)
    {
      Lista::Usun(1);
    }
}
