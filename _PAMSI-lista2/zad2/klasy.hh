//plik naglowkowy przechowujacy definicje klas i prototypy metod
//AUTOR: Mikolaj Zurowski
//NR INDEKSU: 226236
#ifndef KLASY_HH
#define KLASY_HH

class Dane //klasa opisujaca pojedynczy element
{
public:
  int wart;
  Dane *nast;
  Dane() {nast=0;}; //domyslnie element na nic nie wskazuje
};

class Lista //definicja klasy listy
{
public:
  Dane dane;
  Dane *pierwszy;
  Lista() {pierwszy=0;}; //domyslnie lista jest pusta
  void Dodaj(int y);
  void Pokaz();
  void Usun (int nr);
  void Czysc();
};


#endif
