//plik glowny do zadania oblugiwania listy jednokierunkowej
//AUTOR: Mikolaj Zurowski
//NR INDEKSU: 226236

#include <iostream>
#include "klasy.hh"

using namespace std;

int main()
{
  int wyb, element,x,liczba=0;
  Lista lista;
  while(1) //menu
    {
      cout <<"1.Dodaj do listy"<<endl;
      cout <<"2.Pokaz liste"<<endl;
      cout <<"3.Usun element z listy"<<endl;
      cout <<"4.Wyczysc liste"<<endl;
      cout <<"0.Koniec"<<endl;
      cin >>wyb;
      cout <<endl;
      switch(wyb)
	{
	case 1:
	  {
	    cout <<"Ile elementow chcesz dodac?"<<endl;
	    cin >>liczba;
	    lista.Dodaj(liczba);
	    cout <<endl;
	    break;
	  }
	case 2:
	  {
	    lista.Pokaz();
	    cout <<endl;
	    break;
	  }
	case 3:
	  {
	    cout <<"Numer elementu do usuniecia: "<<endl;
	    cin >>x;
	    lista.Usun(x);
	    break;
	  }
	case 4:
	  {
	    lista.Czysc();
	    break;
	  }
	case 0:
	  {
	    return 0;
	  }
	}
    }
}
