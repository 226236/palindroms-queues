//plik cpp zawierajacy definicje wszystkich funkcji. Czesc z funkcji nie jest wykorzystywana w programie
//ze wzgledu na brak ich zastosowania (czyszczenie listy, usuwanie konkretnego elementu (zastapiane usuwaniem
//z porownanie elementow celem usuniecia duplikatu))
//AUTOR: Mikolaj Zurowski
//NR INDEKSU: 226236

#include <iostream>
#include <string>
#include "funkcje.hh"

using namespace std;
int liczbaElementow=0;
Lista ListaPalindromow;

void Lista::Dodaj(string x)  //funkcja dodajaca element do listy
{
      Dane *nowa= new Dane;
      nowa->wart=x;          //nowa zmienna dostaje wartosc wpisywanego wyrazu
      if(pierwszy==0)        //sprawdzamy czy dodajemy na poczatek listy
	{
	  pierwszy=nowa;     //jesli tak, nowy wyraz jest poczatkiem listy
	}
      else
	{
	  Dane *tmp=pierwszy;//pomocniczy wskaznik na poczatek listy
	  while(tmp->nast)   //dopoki wskaznik na poczatek listy nie wskazuje na jej koniec 
	    {
	      tmp=tmp->nast; //przechodzimy do nastepnego elementu
	    }
	  tmp->nast=nowa;    //nowy element staje sie poczatkiem listy
	  nowa->nast=0;
	}
}

void Lista::Pokaz()             //funkcja pokazujaca zawartosc listy
{
  Dane *tmp=pierwszy;
  while(tmp)                    //dopoki wskaznik na pierwszy element nie bedzie wskazywal NULL, instrukcja 
    {                           //bedzie sie wykonywac  
      cout << tmp->wart<<endl;  //wypisanie wartosci konkretnego elementu
      tmp=tmp->nast;            //przypisanie wartosci kolejnego elementu
    }
}

void Lista::Usun(int nr) //funkcja usuwajaca dany element z listy. nie uzyta w tym programie
{
  {
    if (nr==1)
    {
        Dane *temp = pierwszy;
        pierwszy = temp->nast; 
    }
    if (nr>=2)
    {
        int j = 1;
        Dane *temp = pierwszy;
	while (temp)
        {
            if ((j+1)==nr)
	      {
		break;
	      }
	    else
	      {
		temp = temp->nast;
		j++;
	      }
        }
        if (temp->nast->nast==0)
	  {
            temp->nast = 0;
	  }
        else
	  {
            temp->nast = temp->nast->nast;
	  }
    }
  }
}

void Lista::Czysc()  //funkcja czyszczaca liste, nie uzyta w tym programie
{
  while(pierwszy!=0)
    {
      Lista::Usun(1);
    }
}

bool jestPal(string wyraz)      //funkcja sprawdzajaca czy ciag znakowy jest palindromem
{
  int i,j;
  int a=wyraz.length();
  for(i=0,j=a-1;i<j;i++,j--)
    {
      if(wyraz[i]!=wyraz[j])    //porownojemy dwa przeciwstawne elementy ciagu. jesli sa rozne, slowo nie jest
	{                       //palindromem, wiec funkcja zwraca wartosc false. jezeli wszystkie elementy
	  return false;         //ciagu beda takie same, to funkcja zwroci wartosc true
	}
    }
}

void swap(char& a, char& b) //funkcja podmieniajaca dwa elementy wyrazu
{
  char temp;                //zmienna pomocnicza
  temp=a;                   //zmienna pomocnicza przyjmuje wartosc pierwszego elementu podmienianego
  a=b;                      //zamiana wartosci elementow
  b=temp;                   //drugi podmieniany element ma wartosc pierwszego  
}

void perm(string s,int i,int n)
{
  int j;
  if(i==n)                           //sprawdzamy czy nasz element sterujacy jest na koncu permutowanego
    {                                //wyrazu 
      if(jestPal(s)==true)           //sprawdzamy czy powstały wyraz jest palindromem. jesli tak, 
	{                            //dodajemy go do globalnej listy palindromow
	   ListaPalindromow.Dodaj(s);
	 }
    }
  else
    { 
      for(j=i;j<s.length();j++)      
	{
	  swap(s[i],s[j]);           //zamieniamy miejscami i-ty i j-ty element wyrazu
	  perm(s,i+1,n);             //rekurencja - zamieniony wyraz poddawany jest mozliwym permutacjom
	  swap(s[i],s[j]);           //zamieniamy zmienione litery jeszcze raz  
	  
	}
    }
}
      
void Lista::UsunDup()
{
  
      Dane *first=pierwszy;
      Dane *drugi=new Dane;
      drugi=first->nast;//przypisanie wartosci drugiego elementu
      while(first->nast!=NULL)//instrukcja bedzie wykonywana dopoki pierwszy element nie bedzie koncem listy
	{
	  if(first->wart==drugi->wart) //sprawdzamy czy wartosc dwoch kolejnych elementow jest taka sama
	    {
	      first->nast=drugi->nast; //wskaznik pierwszego wskazuje na element za drugim
	      drugi->nast=NULL;        //niszczymy wskaznik drugiego elementu
	      delete drugi;            //niszczymy drugi element
	      drugi=first->nast;       //nowy drugi element jest tym na co wskazuje pierwszy
	    }
	  else                         //jezeli wartosci kolejnych elementow sa rozne
	    {
	      first=drugi;             //przesuwamy wskaznik z pierwszego na drugi
	      first->nast=drugi->nast; //wskaznik pierwszego wskazuje tam gdzie drugiego
	      drugi=first->nast;       //drugi jest tym, na co wskazuje pierwszy
	    }
	}
      
      
}



