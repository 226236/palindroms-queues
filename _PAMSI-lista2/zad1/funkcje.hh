//plik z definicjami klas i prototypami funkcji
//AUTOR: Mikolaj Zurowski
//NR INDEKSU: 226236

#ifndef FUNKCJE_HH
#define FUNKCJE_HH

class Dane    //klasa opisujaca pojedynczy element listy
{
public:
  std::string wart;  //sklada sie z wartosci oraz wskaznika na nastepny element
  Dane *nast;
  Dane() {nast=0;};  //konstruktor mowi,ze element domyslnie na nic nie wskazuje
};

class Lista   //klasa opisujaca cala liste
{
public:
  Dane dane;        //sklada sie z elemenentow zadeklarowanych wczesniej i 
  Dane *pierwszy;   //wskaznika na poczatek
  Lista() {pierwszy=0;};  //domyslnie lista jest pusta
  void Dodaj(std::string x);  //metody klasy lista
  void Pokaz();
  void Usun (int nr);
  void Czysc();
  void UsunDup();
};

bool jestPal(std::string wyraz); //prototypy funkcji 
void swap(char& a, char& b);
void perm(std::string s, int i, int n);

extern Lista ListaPalindromow; //deklaracja globalnej listy palindromow

#endif
