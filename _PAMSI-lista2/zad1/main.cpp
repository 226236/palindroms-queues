//plik glowny do programu obslugujacego palindromy
//Funkcje: po dodaniu wyrazu (ciagu znakowego STRING) tworzymy permutacje danego
//ciagu i sprawdzamy, czy permutacja jest palindromem. Jesli tak, dodajemy
//ta permutacje do listy palindromow zadeklarowanej globalnie
//AUTOR: Mikolaj Zurowski
//NR INDEKSU: 226236

#include <iostream>
#include <string>
#include "funkcje.hh"
using namespace std;


int main()
{
  
  std::string wyraz;
  int wyb;
  int i=0;
  while(1)       //wywolanie menu do obslugi programu
    {
      cout <<"1.Wprowadz wyraz"<<endl;
      cout <<"2.Wyswietl liste znalezionych palindromow"<<endl;
      cout <<"3.Usun duplikaty z listy"<<endl;
      cout <<"0.Koniec"<<endl;
      cin>>wyb;
      switch(wyb)
	{
	case 1:
	  {
	    cout<<"Wprowadz: "<<endl;        //wprowadzanie wyrazu i generacja permutacji
	    cin >>wyraz;
	    perm(wyraz,0,wyraz.length()-1);
	    break;
	  }
	case 3:
	  {
	    if(ListaPalindromow.pierwszy==0)     //sprawdzenie czy lista nie jest pusta
	      {
		cout<<"Brak palindromow!"<<endl; //jesli tak -> wyswietlenie komunikatu
		
	      }
	    else
	      {
		ListaPalindromow.UsunDup();      //jesli nie -> usuwamy duplikaty
		
	      }
	    break;
	  }
	case 2:
	  {
	    ListaPalindromow.Pokaz();  //pokazanie listy palindromow
	    break;
	  }
	case 0:
	  {
	    return 0;  //dla wybranego 0 program konczy dzialanie
	    break;
	  }
	}
    }
}
