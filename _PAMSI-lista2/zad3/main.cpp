//plik glowny do obslugi kolejki. Mozna dodac, usunac, pokazac, wyczyscic.
//AUTOR: Mikolaj Zurowski
//NR INDEKSU: 226236
#include <iostream>
#include <cstring>
#include "klasy.hh"
using namespace std;

int main()
{
  int wyb;
  int element;
  Kolejka kolejka;
  while(1)
    {
      cout <<"1. Dodaj"<<endl;
      cout <<"2. Usun"<<endl;
      cout <<"3. Pokaz kolejke"<<endl;
      cout <<"4. Czysc"<<endl;
      cout <<"0.Koniec"<<endl;
      cin >>wyb;
      cout <<endl;
      switch(wyb)
	{
	case 1:
	  {
	    cout <<"Podaj element: "<<endl; //dodanie elementu do kolejki
	    cin >>element;
	    kolejka.Dodaj(element);
	    cout<<endl;
	    break;
	  }
	  case 2:
	  {
	    kolejka.Usun();  //usuwanie elementu z kolejki
	    break;
	  }
	case 3:
	  {
	    kolejka.Pokaz(); //pokazanie kolejki
	    cout <<endl;
	    break;
	  }
	  case 4:
	  {
	    kolejka.Czysc(); //czyszczenie kolejki
	    break;
	  }
	case 0:
	  {
	    return 0; //koniec programu
	    break;
	  }
	}
    }
}
