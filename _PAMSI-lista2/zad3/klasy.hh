//plik naglowkowy opisujacy klasy i metody
//AUTOR: Mikolaj Zurowski
//NR INDEKSU: 226236
#ifndef KLASY_HH
#define KLASY_HH

class elem //pojedynczy element listy
{
public:
  int wartosc;
  elem *nast;
  elem() {nast=0;}; //domyslnie element na nic nie wskazuje
};

class Kolejka  //definicja kolejki
{
public:
  elem element;
  elem *wierzcholek;
  Kolejka() {wierzcholek=0;}; //domyslnie kolejka jest pusta
  void Dodaj(int x);
  void Usun();
  void Pokaz();
  void Czysc();
};

#endif
