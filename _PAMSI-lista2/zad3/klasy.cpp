//definicje metod. Metody sa identyczne jak dla listy jednokierunkowej
//z poprzedniego zadania
//AUTOR: Mikolaj Zurowski
//NR INDEKSU: 226236
#include <iostream>
#include "klasy.hh"
using namespace std;

void Kolejka::Dodaj(int x) //dodawanie do kolejki 
{                          
  elem *nowy=new elem;     
  nowy->wartosc=x;
  if(wierzcholek==0)
    {
      wierzcholek=nowy;
    }
  else
    {
      elem *tmp=wierzcholek;
      while(tmp->nast)
	{
	  tmp=tmp->nast;
	}
      tmp->nast=nowy;
      nowy->nast=0;
    }
}

void Kolejka::Pokaz()  //wyswietlanie kolejki
{
  elem *tmp=wierzcholek;
  while(tmp)
    {
      cout << tmp->wartosc<<endl;
      tmp=tmp->nast;
    }
}
      
void Kolejka::Usun() //usuwanie elementu z kolejki. wyglada inaczej niz w 
{                    //przypadku listy - usuwamy tylko pierwszy dodany element
  if(wierzcholek==NULL)
    {
      delete wierzcholek;
      wierzcholek=NULL;
    }
  else
    {
      elem *tmp=wierzcholek;
      delete wierzcholek;
      wierzcholek=tmp->nast;
    }
}

void Kolejka::Czysc()  //czyszczenie kolejki
{
  while(wierzcholek!=NULL)
    {
      Kolejka::Usun();
      
    }
}
